package com.hcl.assignment3;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;

public class MagicOfBooks<T> {
	Scanner scanner = new Scanner(System.in);

	HashMap<Integer, Book> bookmap = new HashMap<>();
	TreeMap<Double, Book> treemap = new TreeMap<>();
	ArrayList<Book> booklist = new ArrayList<>();

	public void addbook() {

		Book addBook = new Book();
		System.out.println("ENTER A BOOK ID: ");
		addBook.setId(scanner.nextInt());

		System.out.println("ENTER A BOOK NAME: ");
		addBook.setName(scanner.next());

		System.out.println("ENTER A BOOK PRICE: ");
		addBook.setPrice(scanner.nextDouble());

		System.out.println("ENTER A BOOK GENRE: ");
		addBook.setGenre(scanner.next());

		System.out.println("ENTER NUMBER OF COPIES SOLD: ");
		addBook.setNoOfCopiesSold(scanner.nextInt());

		System.out.println("ENTER A BOOK STATUS: ");
		addBook.setBookstatus(scanner.next());

		bookmap.put(addBook.getId(), addBook);
		System.out.println("...BOOK IS ADDED SUCCESSFULLY...");

		treemap.put(addBook.getPrice(), addBook);
		booklist.add(addBook);
	}

	public void deletebook() throws CustomException {
		if (bookmap.isEmpty()) {
			throw new CustomException("****NO BOOKS ARE AVAILABLE TO DELETE***** ");
		} else {
			System.out.println("ENTER BOOK ID YOU WANT TO DELETE: ");
			int id = scanner.nextInt();
			bookmap.remove(id);
			System.out.println("...SUCCESSFULLY DELETED THE BOOK....");
		}
	}

	public void updatebook() throws CustomException {
		if (bookmap.isEmpty()) {
			throw new CustomException("***NO BOOKS ARE AVAILABLE TO UPDATE***");
		} else {
			Book updateBook = new Book();
			System.out.println("ENTER A BOOK ID");
			updateBook.setId(scanner.nextInt());

			System.out.println("ENTER A BOOK NAME");
			updateBook.setName(scanner.next());

			System.out.println("ENTER A BOOK PRICE");
			updateBook.setPrice(scanner.nextDouble());

			System.out.println("ENTER A BOOK GENRE");
			updateBook.setGenre(scanner.next());

			System.out.println("ENTER NUMBER OF COPIES SOLD");
			updateBook.setNoOfCopiesSold(scanner.nextInt());

			System.out.println("ENTER A BOOK STATUS");
			updateBook.setBookstatus(scanner.next());

			bookmap.replace(updateBook.getId(), updateBook);
			System.out.println("...BOOK DETAILS ARE UPDATED SUCCESSFULLY...");
		}
	}

	public void displayBookInfo() throws CustomException {

		if (bookmap.size() > 0) {
			Set<Integer> keySet = bookmap.keySet();

			for (Integer key : keySet) {

				System.out.println(key + " ----> " + bookmap.get(key));
			}
		} else {
			throw new CustomException("***BOOKMAP IS EMPTY***");
		}

	}

	public void count() throws CustomException {
		if (bookmap.isEmpty()) {
			throw new CustomException("BOOK STORE IS EMPTY.");
		} else {
			System.out.println("NUMBER OF BOOKS PRESENT IN A STORE " + bookmap.size());
		}
	}

	public void autobiography() throws CustomException {
		String bestSelling = "Autobiography";
		if (booklist.isEmpty()) {
			throw new CustomException("****BOOK STORE IS EMPTY***");
		} else {
			ArrayList<Book> genreBookList = new ArrayList<Book>();

			Iterator<Book> it = (booklist.iterator());

			while (it.hasNext()) {
				Book book1 = (Book) it.next();
				if (book1.genre.equals(bestSelling)) {
					genreBookList.add(book1);
					System.out.println(genreBookList);
				}
			}
		}
	}

	public void displayByFeature(int flag) throws CustomException {
		if (flag == 1) {
			if (treemap.size() > 0) {
				Set<Double> keySet = treemap.keySet();
				for (Double key : keySet) {
					System.out.println(key + " ----> " + treemap.get(key));
				}
			} else {
				throw new CustomException("***SORRY!BOOK STORE IS EMPTY****");
			}
		}
		if (flag == 2) {
			Map<Double, Object> treeMapReverseOrder = new TreeMap<>(treemap);
			NavigableMap<Double, Object> navigablemap = ((TreeMap<Double, Object>) treeMapReverseOrder).descendingMap();
			System.out.println("BOOK DETAILS ARE: ");
			if (navigablemap.size() > 0) {
				Set<Double> keySet = navigablemap.keySet();
				for (Double key : keySet) {
					System.out.println(key + " ----> " + navigablemap.get(key));
				}
			} else {
				throw new CustomException("***SORRY BOOK STORE IS EMPTY***");
			}
		}

		if (flag == 3) {
			String bestSelling = "BOOKS";
			ArrayList<Book> genreBookList = new ArrayList<Book>();

			Iterator<Book> it = booklist.iterator();

			while (it.hasNext()) {
				Book b = (Book) it.next();
				if (b.bookStatus.equals(bestSelling)) {
					genreBookList.add(b);

				}

			}
			if (genreBookList.isEmpty()) {
				throw new CustomException(" SORRY! NO BEST SELLING BOOKS ARE AVAILABLE!");
			} else
				System.out.println(" BEST SELLING BOOKS ARE: " + genreBookList);

		}
	}
}
