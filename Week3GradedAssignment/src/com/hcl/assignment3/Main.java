package com.hcl.assignment3;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		MagicOfBooks magicbooks = new MagicOfBooks();

		while (true) {
			System.out.println("1. ADD A NEW BOOK\r\n" + "2. DELETE A BOOK\r\n" + "3. UPDATE A BOOK\r\n"
					+ "4. DISPLAY ALL THE BOOKS \r\n" + "5. TOTAL COUNT OF THE BOOKS\r\n"
					+ "6. SEARCH FOR AUTOBIOGRAPHY BOOKS\r\n" + "7. DISPLAY THE BOOKS BY ITS FEATURES");

			System.out.println(" ENTER YOUR CHOICE:");
			int choice = scanner.nextInt();

			switch (choice) {

			case 1:// Adding a Book
				System.out.println(" ENTER  NUMBER OF BOOKS YOU WANT TO ADD: ");
				int a = scanner.nextInt();
				for (int i = 1; i <= a; i++) {
					magicbooks.addbook();
					}
				break;

			case 2:// Deleting a Book
				try {
					magicbooks.deletebook();
				} catch (CustomException ce) {
					System.out.println(ce.getMessage());
				}
				break;

			case 3://Updating a Book
				try {
					magicbooks.updatebook();
				} catch (CustomException ce) {
					System.out.println(ce.getMessage());
				}
				break;

			case 4: // Displaying a Book
				try {
					magicbooks.displayBookInfo();
				} catch (CustomException ce) {
					System.out.println(ce.getMessage());
				}
				break;

			case 5: // No.of Count For all the Books
				System.out.println("COUNT OF ALL THE BOOKS: ");
				try {
					magicbooks.count();
				} catch (CustomException ce) {
					System.out.println(ce.getMessage());
				}
				break;

			case 6: // Searching Autobiography Books
				try {
					magicbooks.autobiography();
				} catch (CustomException ce) {
					System.out.println(ce.getMessage());
				}
				break;

			case 7: // Displaying a Book by its features
				System.out.println(
						"ENTER YOUR CHOICE: \n 1. PRICE LOW TO HIGH " + "\n 2.PRICE HIGH TO LOW \n 3. BEST SELLING");
				int ch = scanner.nextInt();

				switch (ch) {

				case 1:
					try {
						magicbooks.displayByFeature(1);
					} catch (CustomException ce) {
						System.out.println(ce.getMessage());
					}
					break;
				case 2:
					try {
						magicbooks.displayByFeature(2);
					} catch (CustomException ce) {
						System.out.println(ce.getMessage());
					}
					break;
				case 3:
					try {
						magicbooks.displayByFeature(3);
					} catch (CustomException ce) {
						System.out.println(ce.getMessage());
					}
					break;
				}
			default:
				System.out.println("***SORRY! YOU HAVE ENTERED WRONG CHOICE****");
			}
		}
	}
}
